<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="sk">
  <head>  
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />   
    <link rel="stylesheet" href="../common/css/cncstyle.css" type="text/css"/>
    <link rel="stylesheet" href="css/cnchome.css" type="text/css"/>
    <script type="text/javascript" src="../common/js/scripts.js"></script>		
    <title>Cognition and Neural Computation Group</title>
  </head>   
  <body>
  		<div id="wrap11">
			<div id="wrap12">	
	  			<div id="header">
					<!--<h1><b>C</b>ognition and <b>N</b>eural <b>C</b>omputation Group</h1>-->
					<h1><a href="?" class="heading">Cognition and Neural Computation group</a></h1>
					<a href="http://www.meicogsci.eu/"><img src="./images/meicogsci.png" class="pt"/></a>
					<a href="http://uniba.sk/"><img src="./images/comenius-new-invert-transparent.png"/></a>
					<div class="clear"></div>		
				</div>
           </div>
      </div>
		<div id="wrap21">
			<div id="wrap22">   
				
				<div id="navi">
			<?php
			     $r_navidata = mysql_query("SELECT name,getname FROM ".$mainTable." WHERE primarytext = '1' ORDER BY id ASC");
			     if ($r_navidata) {
			          echo '<ul>'."\n";
			          while ($row = mysql_fetch_assoc($r_navidata)) {
			               if ($row['getname'] == "home") 
			                    echo '<li><a href="?">'.$row['name'].'</a></li>'."\n";
		                    else 
		                         echo '<li><a href="?action='.$row['getname'].'">'.$row['name'].'</a></li>'."\n"; 
			          }
	       ?>
				</ul>
			<?php 
			          mysql_free_result($r_navidata);
			     }
               ?>
		       		<div class="clear"></div>
           		</div>
				<img src="images/cncdeco.png" width="900"/>   
				<div id="content">
			<?php
                    $q = "";
                    $a = "";
                    if (isset($_GET['action'])) {
                         $q = "SELECT * FROM ".$mainTable." WHERE getname = '".mysql_real_escape_string($_GET['action'])."'";
                         $a = $_GET['action'];
                    } else {
                         $q = "SELECT * FROM ".$mainTable." WHERE id = 1";
                    }
                    if ($a == "publications")
                        include "publications.php";
                    if ($a == "research")
                        include "projects.php";
					else {
						$r_mainbody = mysql_query($q);
						if ($r_mainbody) {
							echo mysql_result($r_mainbody,0,"text")."\n"; 
							mysql_free_result($r_mainbody);
						}
					}
			?>
               </div> 
               <div class="clear"></div>	
               <div id="footer">
           <?php
           		$q = "SELECT * FROM ".$mainTable." WHERE name = 'footer'";
           		$r_footer = mysql_query($q);
           		if ($r_footer && mysql_num_rows($r_footer) != 0) {
           			echo mysql_result($r_footer,0,"text")."\n";
           			mysql_free_result($r_footer);
           		}
           ?>
               </div>
		</div>
	</div>
  </body>
</html>
